# GIR Solutions Dashboard

### About
React (v16.7+) based dashboard built with reactstrap (Boostrap 4.2.1) for GIR Solutions Level 1 reporting
software. This dashboard takes advantage of the Stitch API for lightning-fast calls to provide up-to-the-minute
data to power charts, live platform search, completion status, notifications, and more.
