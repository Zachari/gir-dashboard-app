const axios = require('axios');

const TEST_SUB_URL = `http://localhost:4000`;

export {
    getRatingsInfo,
    generateRatingsGraph
}

function getRatingsInfo() {
   
        // console.log(`API URI: ${BASE_SUB_URL}?username=zac%40zachari.io&password=LillianDuck032015&submission_guid=${guid}`)
        const value =  axios.get(`${TEST_SUB_URL}/ratings/count`).then(res => res.data);
        return value;
    
}

async function generateRatingsGraph() {
    const ratings = await getRatingsInfo().then(res => res);
    console.log(ratings);
    const total = [ratings.total.good, ratings.total.fair, ratings.total.poor];
    localStorage.setItem('urgentCount', ratings.total.poor);
    
    return {
        data: canvas => {
          let ctx = canvas.getContext("2d");
      
          let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);
      
          gradientStroke.addColorStop(1, "rgba(72,72,176,0.1)");
          gradientStroke.addColorStop(0.4, "rgba(72,72,176,0.0)");
          gradientStroke.addColorStop(0, "rgba(119,52,169,0)"); //purple colors
          
          return {
            labels: ["GOOD", "FAIR", "POOR"],
            datasets: [
              {
                label: "Condition",
                fill: true,
                backgroundColor: gradientStroke,
                hoverBackgroundColor: gradientStroke,
                borderColor: "#d048b6",
                borderWidth: 2,
                borderDash: [],
                borderDashOffset: 0.0,
                data: total
              }
            ]
          };
        },
        options: {
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          tooltips: {
            backgroundColor: "#f5f5f5",
            titleFontColor: "#333",
            bodyFontColor: "#666",
            bodySpacing: 4,
            xPadding: 12,
            mode: "nearest",
            intersect: 0,
            position: "nearest"
          },
          responsive: true,
          scales: {
            yAxes: [
              {
                gridLines: {
                  drawBorder: false,
                  color: "rgba(225,78,202,0.1)",
                  zeroLineColor: "transparent"
                },
                ticks: {
                  suggestedMin: 0,
                  suggestedMax: 5000,
                  padding: 20,
                  fontColor: "#9e9e9e"
                }
              }
            ],
            xAxes: [
              {
                gridLines: {
                  drawBorder: false,
                  color: "rgba(225,78,202,0.1)",
                  zeroLineColor: "transparent"
                },
                ticks: {
                  padding: 20,
                  fontColor: "#9e9e9e"
                }
              }
            ]
          }
        }
      };

      
      
}

