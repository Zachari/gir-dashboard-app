import React, { Component } from 'react'
import {
    ListGroup,
    ListGroupItem
} from 'reactstrap';

const divStyle = {
    color: 'black'
};

class Suggestions extends Component {
    state = {
        results: this.props.results,
        visible: false
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.results !== this.props.results) {
            this.setState({
                results: nextProps.results
            });
        }
        
        if(nextProps.results.length >= 1) {
            this.setState({
                visible: true
            })
        } else {
            this.setState({
                visible: false
            })
        }
    }

    getOptions = () => {
        
        const options = this.state.results.map(r => (
                <ListGroupItem tag="button" key={r.platform} onClick={(e) => this.props.suggestionHandler(e, r.platform_name)}>
                    {r.platform_name}
                </ListGroupItem> 
        ));
         
        return options;
    }
    render() {
        return (
            <div>
                { this.state.visible ? 
                    <div className="modal-body">
                    <ListGroup flush>
                        {
                            this.state.results ?
                                this.getOptions() : null
                        }
                    </ListGroup>
                </div>
                : null
            }
            </div>
            
        );
    }
}

export default Suggestions;