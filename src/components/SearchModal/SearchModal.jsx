import React, { Component } from "react";
import Suggestions from './Suggestions';
// nodejs library that concatenates classes
import classNames from "classnames";

// reactstrap components
import {
    Input,
    Modal,
} from "reactstrap";
import axios from 'axios';
const API_URL = `http://localhost:4000/search`;

class SearchModal extends Component {
    state = {
        query: '',
        results: [],
        modalSearch: this.props.isOpen,
        suggestionsVisible: false
    };

    suggestionSelectionHandler = (event, result) => {
        event.preventDefault();

        this.setState({ query: result.platform_name });
        this.handleInputChange();
    }

    // this function is to open the Search modal
    toggleModalSearch = () => {
        this.setState({
            modalSearch: !this.state.modalSearch
        });
    };

    getInfo = () => {
        axios.get(`${API_URL}/${this.state.query}/limit/7`).then((data) => {
            let d = data.data.sort((a,b) => {
                var textA = a.platform_name.toUpperCase();
                var textB = b.platform_name.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
            console.log(d)
            this.setState({
                results: d
            });
        });
    }

    handleInputChange = () => {
        this.setState({
            query: this.search.value
        }, () => {
            if (this.state.query && this.state.query.length > 1) {
                if (this.state.query.length % 1 === 0) {
                    this.getInfo();
                }
            }
        });
    }

    handleSuggestion = (e, data) => {
        console.log(data);
        this.setState({
            query: data
        }, () => {
            this.getInfo();  
        });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.isOpen !== this.props.isOpen) {
            this.setState({
                modalSearch: nextProps.isOpen
            });
        }
    }

    render() {
        return (
            <Modal
                modalClassName="modal-search"
                isOpen={this.state.modalSearch}
                toggle={this.toggleModalSearch}
            >
                <div className="modal-header">
                    <Input id="inlineFormInputGroup" placeholder="SEARCH" type="text" value={this.state.query} innerRef={input => this.search = input} onChange={this.handleInputChange} />
                    <button
                        aria-label="Close"
                        className="close"
                        data-dismiss="modal"
                        type="button"
                        onClick={this.toggleModalSearch}
                    >
                        <i className="tim-icons icon-simple-remove" />
                    </button>
                </div>
                
                <Suggestions results={this.state.results} suggestionHandler={this.handleSuggestion}/>
                
                
                
                
            </Modal >
        );
    }
}

export default SearchModal;  